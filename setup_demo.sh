#!/bin/bash

echo "Checking dependencies"
command -v docker >/dev/null 2>&1 || { echo >&2 "Docker is required but not installed.  Aborting."; exit 1; }
command -v kind >/dev/null 2>&1 || { echo >&2 "Kind is required but not installed.  Aborting."; exit 1; }
command -v kubectl >/dev/null 2>&1 || { echo >&2 "Kubectl is required but not installed.  Aborting."; exit 1; }
command -v jq >/dev/null 2>&1 || { echo >&2 "jq is required but not installed.  Aborting."; exit 1; }
command -v envsubst >/dev/null 2>&1 || { echo >&2 "envsubst is required but not installed.  Aborting."; exit 1; }
command -v base64 >/dev/null 2>&1 || { echo >&2 "base64 is required but not installed.  Aborting."; exit 1; }
command -v sed >/dev/null 2>&1 || { echo >&2 "sed is required but not installed.  Aborting."; exit 1; }
echo "All dependencies are installed."

echo "Creating cluster"
kind create cluster --name fenrir-1 --config kind-config.yaml --kubeconfig $HOME/.kube/fenrir-1

echo "Running some commands to make sure the cluster is ready"
export KUBECONFIG=$HOME/.kube/fenrir-1
kubectl cluster-info
kubectl get nodes

# Avoid scheduling pods on worker2
kubectl cordon fenrir-1-worker2

echo "########### Setup MetalLB ###########"
echo "* Installing MetalLB"
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
echo "* Waiting for MetalLB to be ready"
kubectl wait --namespace metallb-system --for=condition=ready pod --selector=app=metallb

echo "* Get IPAM config: "
docker network inspect kind
export IP_PREFIX=$(docker network inspect kind | jq -r '.[].IPAM.Config[]?.Subnet' | grep -Eo '^[0-9]+\.[0-9]+\.[0-9]+\.' | head -n 1)
echo "* IP Prefix: $IP_PREFIX"
export IP_RANGE_START="$IP_PREFIX.30"
export IP_RANGE_END="$IP_PREFIX.80"
echo "* IP Range: $IP_RANGE_START - $IP_RANGE_END"
cat metallb/pools.yaml | envsubst | kubectl apply -f -

echo "########### Installing ArgoCD ###########"
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
echo "* Exposing ArgoCD"
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

while [[ -z $(kubectl get svc -n argocd argocd-server -o jsonpath="{.status.loadBalancer.ingress}" 2>/dev/null) ]]; do
  echo "** still waiting for argocd/argocd-server to get ingress"
  sleep 1
done
echo "* argocd/argocd-server now has ingress."

export ARGOCD_IP=$(kubectl -n argocd get svc argocd-server -o json | jq -r .status.loadBalancer.ingress[0].ip)

echo "* Waiting for ArgoCD to be ready"
kubectl wait pod --all --for=condition=Ready --namespace argocd

# Allow scheduling pods on worker2
kubectl uncordon fenrir-1-worker2

echo "#################################################"
echo "You're ready to go!"
echo "ArgoCD is ready at http://$ARGOCD_IP:443"
echo "ArgoCD login: admin / $(kubectl -n argocd get secrets argocd-initial-admin-secret -o json | jq -r .data.password | base64 -d)"
echo "#################################################"
