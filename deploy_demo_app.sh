echo "Deploy Demo app"
kubectl apply -f app.yaml
sleep 5
kubectl wait pod --all --for=condition=Ready --namespace docker-demoapp

while [[ -z $(kubectl get svc -n docker-demoapp docker-demoapp -o jsonpath="{.status.loadBalancer.ingress}" 2>/dev/null) ]]; do
  echo "still waiting for docker-demoapp/docker-demoapp to get ingress"
  sleep 1
done
echo "docker-demoapp/docker-demoappnow has ingress."
export DEMOAPP_IP=$(kubectl -n docker-demoapp get svc docker-demoapp -o json | jq -r .status.loadBalancer.ingress[0].ip)

echo "Demo app is ready at http://$DEMOAPP_IP:5000"