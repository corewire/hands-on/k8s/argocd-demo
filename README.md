# Argocd Demo

## Preparation (10 - 15 minutes)

### dependencies
- docker
- kind
- kubectl
- jq
- envsubst
- base64
- sed

### Setup demo environment:

```bash
$ bash setup_demo.sh
```

The script will information similar to:
```
#################################################
You're ready to go!
ArgoCD is ready at http://172.22.0.30:443
ArgoCD login: admin / 24qp2j7kpTSl3Grw
Demo app is ready at http://172.22.0.31:5000
#################################################
```

Make yourself familiar with the ArgoCD UI.

#### What does the script do?
This script serves as a setup guide to create a Kubernetes cluster using `kind`, install `ArgoCD` for Continuous Deployment (CD), and setup `MetalLB` as a LoadBalancer solution for kind.

1. **Check for Dependencies**: Initially, the script checks if the necessary dependencies (`docker`, `kind`, `kubectl`, `jq`, `envsubst`, `base64`, `sed`) are installed on your machine. If any of these dependencies are missing, the script will abort with an error message.

2. **Create a Kubernetes Cluster**: The script creates a new Kubernetes cluster named `fenrir-1` using `kind`, based on the configuration provided in `kind-config.yaml`. The kubeconfig for this cluster is stored at `$HOME/.kube/fenrir-1`.

3. **Verify Cluster Creation**: After the cluster is set up, the script runs a few `kubectl` commands (`cluster-info`, `get nodes`) to verify the proper setup of the cluster.

4. **Install ArgoCD**: The script installs `ArgoCD` into the `argocd` namespace using a publicly available manifest file. It changes the service type of `argocd-server` to `LoadBalancer` and waits until an IP address has been provisioned for the LoadBalancer.

5. **Set up MetalLB**: Next, the script installs `MetalLB`, a load balancer solution for bare metal Kubernetes clusters, from the publicly available manifest files. It waits until the MetalLB pods are ready in the `metallb-system` namespace.

6. **Configure MetalLB**: Finally, the script retrieves the subnet used by the `kind` Docker network, extracts the IP prefix from this subnet, and defines an IP range for MetalLB to use when assigning external IPs to services.

By the end of this script, your Kubernetes cluster is set up and ready to go, with ArgoCD installed for continuous deployment, and MetalLB set up to assign external IPs to services.


## Demo

### Part 1: Deploying the demo application
1. Show the demo deployment: https://gitlab.com/corewire/hands-on/k8s/deploy-docker-demoapp.git
   - The application is deployed in the namespace `docker-demoapp`
   - The application consists of a deployment of the web application and a database
   - The file `frontend.yaml` contains the deployment of the web application as well as a service such that the application can be accessed from outside the cluster.
   - The file `db.yaml` contains the deployment of the database as well as a service such that the web application can access the database.
1. Open the ArgoCD UI in your browser and quickly explain what argocd is:
   - ArgoCD is a declarative, GitOps continuous delivery tool for Kubernetes.
   - ArgoCD watches a Git repository for changes and deploys the application to the cluster.
   - ArgoCD also provides a nice UI to see the status of the application.
1. Deploy the demo application for argocd:
  ```bash
   $ kubectl apply -f app.yaml
  ```
  or
  ```
  $ bash deploy_demo_app.sh
  ```
  the argocd application `Application` we just created  will tell argocd to deploy the application from the Git repository
1. Click on `Applications` in the left menu
1. Click on `docker-demoapp` in the list of applications
   - We can see that the application has been deployed successfully
   - We can see that the application is deployed in the namespace `docker-demoapp`
1. Get ip + port of demo application:
    ```bash
    $ kubectl --namespace docker-demoapp get svc docker-demoapp
    ```
1. Open the demo application in your browser by using the external ip and port 5000,

### Part 2: Scale the application
1. In the `Applications` view, click on `docker-demoapp`
1. Click on `Pods` in the top right menu:
   - TODO: image
1. In this view we can see all the pods that are part of the application and on which node they are running.
1. Scale the demo application to 3 replicas:
   ```bash
   $ kubectl scale --replicas=3 deployment/docker-demoapp -n docker-demoapp
   ```
1. Go back to the `Pods` view in ArgoCD
   - We can see that the new pods are running on different nodes and not all on the same node.
1. Scale the database to 2 replicas:
   ```bash
   $ kubectl scale --replicas=2 deployment/database -n docker-demoapp
   ```
1. Go back to the `Pods` view in ArgoCD.
    - We can see that the new pods are running on different nodes and not all on the same node.

### Part 3: Simulate a failover scenario using:

We want to simulate a failover scenario where one of the nodes is not available anymore.
To safe time we will simulate this by cordoning and draining the node.
We could also simulate this by shutting down the node, however this would take more time, because we would have to wait for the node to be marked as not ready and the pods to be evicted, which would take about 5-6 minutes.

1. Cordon worker 2 to avoid new pods being scheduled on it:
    ```bash
    $ kubectl cordon fenrir-1-worker2
    ```
1. Drain worker 2 to evict the pods running on it:
    ```bash
    $ kubectl drain --ignore-daemonsets --delete-emptydir-data  fenrir-1-worker2
    ```
1. uncordon node 2 to allow new pods being scheduled on it:
    ```bash
    $ kubectl uncordon fenrir-1-worker2
    ```

